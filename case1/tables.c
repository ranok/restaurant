#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <limits.h>
#include <string.h>

#define PRANDOM 0
#define DEFAULT_NUM_TABLES 10
typedef char person;

struct table {
  int capacity; // Implicitly 16
  int num_seated;
  person *guests;
};

const char *restaurant_name = "Chez Jacob";

int poissonRandom(double expectedValue);
int min(int a, int b);

size_t setup_tables(struct table *tables, size_t num_tables);
size_t get_party();
size_t seat_guests(struct table *table, size_t party_sz);
void clean_table(struct table *table);
void print_state(struct table *tables, size_t num_tables);

int main(int argc, char ** argv) 
{
  int num_tables = DEFAULT_NUM_TABLES;
  struct table *tables = NULL;
  int num_parties;

  if (PRANDOM)
    srand(time(0));

  if (argc == 2) {
    num_tables = atoi(argv[1]);
  }
  // Assume there are no more than 4 full seatings per night
  num_parties = rand() % (4 * num_tables);

  printf("Allocating %d tables for %s\n", num_tables, restaurant_name);
  tables = (struct table *) malloc(sizeof(struct table) * num_tables);
  if (!tables) {
    fprintf(stderr, "Unable to allocate restaurant!\n");
    return -1;
  }

  printf("The restaurant is open!\n");

  do {
    setup_tables(tables, num_tables);
    printf("There are %d parties lined up to eat\n", num_parties);
    
    unsigned toseat = min(num_parties, num_tables);

    for (int i = 0; i < toseat; i++) {
      size_t party_sz = get_party();
      size_t seated = seat_guests(&tables[i], party_sz);
      if (seated != party_sz) {
	fprintf(stderr, "Party of size %lu was not properly seated (only %lu seated)!\n", party_sz, seated);
	goto error;
      }
    }
    num_parties -= toseat;
    printf("Seated %d parties so far, %d in waiting area!\n", toseat, num_parties);
    print_state(tables, num_tables);

    for (int i = 0; i < toseat; i++) {
      clean_table(&tables[i]);
    }

  } while (0 != num_parties);

 error:
  for (int i = 0; i < num_tables; i++) {
    clean_table(&tables[i]);
  }
  free(tables);
  return 0;
}

// Returns a party size of between 1-16 (implicitly assuming largest party is 16)
size_t get_party() 
{
  // No zero size parties!
  size_t partysz; 
  do {
    partysz = (poissonRandom(4.0) % 16);
  } while (0 == partysz);
  //printf("Party of size %lu just arrived to %s!\n", partysz, restaurant_name);
  return partysz;
}

size_t seat_guests(struct table *table, size_t party_sz)
{
  const char *partystr = "WHOOPARTYTIME!!!";
  // All tables are size 16
  table->num_seated = 0;
  table->guests = (person *) malloc(sizeof(person) * party_sz);
  if (!table->guests) {
    fprintf(stderr, "Unable to allocate party to table!\n");
    return 0;
  }
  strncpy(table->guests, partystr, 16);
  table->num_seated = party_sz;

  return table->num_seated;
}

// Free()s the guests at a party and resets it to be empty
void clean_table(struct table *table)
{
  if (table->num_seated) {
    free(table->guests);
    table->num_seated = 0;
  }
}

// Setups the restaurant for dining
size_t setup_tables(struct table *tables, size_t num_tables) 
{
  size_t tables_setup = 0;
  for (unsigned int i = 0; i < num_tables; i++) {
    tables[i].capacity = 16;
    tables[i].num_seated = 0;
    tables[i].guests = NULL;
    tables_setup++;
  }

  return tables_setup;
}

unsigned int total_space = 0, used_space = 0;

// Prints out the stats for the night at the restaurant
void print_state(struct table *tables, size_t num_tables)
{
  float eff;
  for (unsigned int i = 0; i < num_tables; i++) {
    if (0 != tables[i].num_seated) {
      total_space += tables[i].capacity;
      used_space += tables[i].num_seated;
    }
  }
  eff = (float) used_space / (float) total_space;
  printf("Cumulative restaurant state: %u seats filled of %u total seats made unusable (%f%% efficiency)\n", used_space, total_space, eff * 100.0);
}

int poissonRandom(double expectedValue) 
{
  int n = 0; //counter of iteration
  double limit; 
  double x;  //pseudo random number
  limit = exp(-expectedValue);
  x = rand() / (double) INT_MAX; 

  while (x > limit) {
    n++;
    x *= rand() / (double) INT_MAX;
  }
  return n;
}

int min(int a, int b)
{
  return (a < b) ? a : b;
}
