#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <limits.h>
#include <string.h>

#define PRANDOM 0
#define VERBOSE 0
#define DEFAULT_NUM_TABLES 10
typedef char person;

struct table {
  int capacity; // Implicitly 12
  int num_seated;
  person *guests;
};

const char *restaurant_name = "Chez Jacob";

int poissonRandom(double expectedValue);
int min(int a, int b);

size_t setup_tables(struct table *tables, size_t num_tables);
size_t get_party();
size_t seat_guests(struct table *table, size_t party_sz);
void clean_table(struct table *table);
void print_state(struct table *tables, size_t num_tables, unsigned int num_parties, unsigned int broken_parties);

int main(int argc, char ** argv) 
{
  unsigned int num_tables = DEFAULT_NUM_TABLES;
  struct table *tables = NULL;
  unsigned int num_parties, seated = 0, broken_parties = 0;

  if (PRANDOM)
    srand(time(0));

  if (argc == 2) {
    num_tables = atoi(argv[1]);
  }
  // Assume there are no more than 4 full seatings per night
  num_parties = rand() % (4 * num_tables);
  size_t *parties = (size_t *) malloc(sizeof(size_t) * num_parties);

  for (unsigned int i = 0; i < num_parties; i++) {
    parties[i] = get_party();
  }

  printf("Allocating %d tables for %s\n", num_tables, restaurant_name);
  tables = (struct table *) malloc(sizeof(struct table) * num_tables);
  if (!tables) {
    fprintf(stderr, "Unable to allocate restaurant!\n");
    return -1;
  }

  printf("The restaurant is open!\n");

  do {
    setup_tables(tables, num_tables);
    printf("There are %d parties lined up to eat\n", num_parties);
    
    unsigned toseat = min(num_parties - seated, num_tables);

    unsigned int curr_table = 0;
    for (int i = 0; i < toseat; i++) {

      if (VERBOSE) 
	printf("Seating party of size %lu\n", parties[seated + i]);
      size_t tseated = seat_guests(&tables[curr_table], parties[seated + i]);
      if (tseated != parties[seated + i]) {
	if (VERBOSE)
	  fprintf(stderr, "Party of size %lu was not properly seated (only %lu seated)!\n", parties[seated + i], tseated);
	broken_parties++;
	curr_table++;
	tseated = seat_guests(&tables[curr_table], parties[seated + i] - tseated);
      }
      
      if (tables[curr_table].capacity == tables[curr_table].num_seated) {
	// Table filled, onto the next!
	curr_table++;
      }
    }
    seated += toseat;

    printf("Seated %d parties so far, %d in waiting area!\n", seated, num_parties - seated);
    print_state(tables, num_tables, num_parties, broken_parties);

    for (int i = 0; i < num_tables; i++) {
      clean_table(&tables[i]);
    }

  } while (0 != num_parties - seated);

 error:
  for (int i = 0; i < num_tables; i++) {
    clean_table(&tables[i]);
  }
  free(tables);
  free(parties);
  return 0;
}

// Returns a party size of between 1-12 (implicitly assuming largest party is 12)
size_t get_party() 
{
  // No zero size parties!
  size_t partysz; 
  do {
    partysz = (poissonRandom(4.0) % 12) + 1;
  } while (0 == partysz);
  if (VERBOSE) 
    printf("Party of size %lu just arrived to %s!\n", partysz, restaurant_name);
  return partysz;
}

size_t seat_guests(struct table *table, size_t party_sz)
{
  const char *partystr = "PARTYTIME!!!";
  size_t seated = 0;
  
  if (VERBOSE)
    printf("Seating party of %lu ", party_sz);

  // All tables are size 12, largest party is size 12, so a whole table will be ok
  if (table->num_seated == 0) {
    if (VERBOSE)
      printf("at empty table\n");
    table->num_seated = party_sz;
    seated = party_sz;
  } else { // Table already partially filled!
    if (VERBOSE)
      printf("at table with %d seats already filled\n", table->num_seated);
    seated = min(party_sz, table->capacity - table->num_seated);
    table->num_seated += seated;
  }

  return seated;
}

// Free()s the guests at a party and resets it to be empty
void clean_table(struct table *table)
{
  if (0 != table->num_seated) {
    table->num_seated = 0;
  }
}

// Setups the restaurant for dining
size_t setup_tables(struct table *tables, size_t num_tables) 
{
  size_t tables_setup = 0;
  for (unsigned int i = 0; i < num_tables; i++) {
    tables[i].capacity = 12;
    tables[i].num_seated = 0;
    tables[i].guests = NULL;
    tables_setup++;
  }

  return tables_setup;
}

unsigned int total_space = 0, used_space = 0;

// Prints out the stats for the night at the restaurant
void print_state(struct table *tables, size_t num_tables, unsigned int num_parties, unsigned int broken_parties)
{
  float eff;
  for (unsigned int i = 0; i < num_tables; i++) {
    if (0 != tables[i].num_seated) {
      total_space += tables[i].capacity;
      used_space += tables[i].num_seated;
    }
  }
  eff = (float) used_space / (float) total_space;
  printf("Cumulative restaurant state: %u seats filled of %u total seats made unusable (%f%% efficiency) %d broken up parties (%f%%)\n", used_space, total_space, eff * 100.0, broken_parties, ((float) broken_parties / (float) num_parties) * 100.0);
}

int poissonRandom(double expectedValue) 
{
  int n = 0; //counter of iteration
  double limit; 
  double x;  //pseudo random number
  limit = exp(-expectedValue);
  x = rand() / (double) INT_MAX; 

  while (x > limit) {
    n++;
    x *= rand() / (double) INT_MAX;
  }
  return n;
}

int min(int a, int b)
{
  return (a < b) ? a : b;
}
