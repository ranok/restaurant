#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <omp.h>
#include <math.h>
#include <limits.h>
#include <string.h>

#define PRANDOM 0
#define VERBOSE 0
#define DEFAULT_NUM_TABLES 10
typedef char person;

struct table {
  int capacity;
  int num_seated;
  person *guests;
};

const char *restaurant_name = "Chez Jacob";

int poissonRandom(double expectedValue);
int min(int a, int b);
int max(int a, int b);

size_t setup_tables(struct table *tables, size_t num_tables);
size_t get_party();
size_t seat_guests(struct table *table, size_t party_sz);
int * calculate_seating_assignments(struct table *tables, unsigned int num_tables, size_t *parties, unsigned int toseat);
unsigned int is_satisfiable(struct table *tables, unsigned int num_tables, size_t *parties, unsigned int toseat);
void clean_table(struct table *table);
void print_state(struct table *tables, size_t num_tables);

int main(int argc, char ** argv) 
{
  unsigned int num_tables = DEFAULT_NUM_TABLES;
  struct table *tables = NULL;
  unsigned int num_parties, seated = 0;

  if (PRANDOM)
    srand(time(0));

  if (argc == 2) {
    num_tables = atoi(argv[1]);
  }
  // Assume there are no more than 4 full seatings per night
  num_parties = rand() % (4 * num_tables);

  printf("Allocating %d tables for %s\n", num_tables, restaurant_name);
  tables = (struct table *) malloc(sizeof(struct table) * num_tables);
  if (!tables) {
    fprintf(stderr, "Unable to allocate restaurant!\n");
    return -1;
  }

  for (unsigned int i = 0; i < num_tables; i++) {
    tables[i].capacity = 0;
  }

  setup_tables(tables, num_tables);
  printf("The restaurant is open!\n");
  
  unsigned int max_party_sz = 0, max_table_sz = 0;
  for (unsigned int i = 0; i < num_tables; i++) {
    if (tables[i].capacity > max_table_sz) {
      max_table_sz = tables[i].capacity;
    }
  }

  //printf("Max table size %d\n", max_table_sz);

  size_t *parties = (size_t *) malloc(sizeof(size_t) * num_parties);
  for (unsigned int i = 0; i < num_parties; i++) {
    parties[i] = min(max_table_sz, get_party());
    if (parties[i] > max_party_sz) {
      max_party_sz = parties[i];
    }
  }

  //printf("Max party size %d\n", max_party_sz);
  assert(max_party_sz <= max_table_sz);

  // Start seating!
  do {
    setup_tables(tables, num_tables);
    printf("There are %d parties lined up to eat\n", num_parties);
    
    unsigned int toseat = min(num_parties, num_tables);
    toseat = is_satisfiable(tables, num_tables, &parties[seated], toseat);
      
    if (toseat != min(num_parties, num_tables)) {
      printf("\x1B[31m" "Leaving %d table(s) empty to maximize efficiency for this seating!\n" "\x1B[0m", min(num_parties, num_tables) - toseat);
    }

    unsigned int *map = calculate_seating_assignments(tables, num_tables, &parties[seated], toseat);

    unsigned char serror = 0;
#pragma omp parallel
#pragma omp for
    for (int i = 0; i < num_tables; i++) {
      if (map[i] != -1) {
	
	size_t tseated = seat_guests(&tables[i], parties[map[i]]);
	if (tseated != parties[map[i]]) {
	  fprintf(stderr, "Party of size %lu was not properly seated (only %lu seated)!\n", parties[map[i]], tseated);
	  serror = 1;
	} else {
	  if (VERBOSE)
	    printf("Party of size %lu was seated at a table of size %u\n", parties[map[i]], tables[i].capacity);
	}
      }
    }

    if (1 == serror)
      goto error;
    free(map);

    num_parties -= toseat;
    seated += toseat;
    printf("Seated %d/%d parties so far, %d in waiting area!\n", toseat, seated, num_parties);
    print_state(tables, num_tables);

    for (int i = 0; i < num_tables; i++) {
      clean_table(&tables[i]);
    }

  } while (0 != num_parties);

  free(parties);
  
 error:
  for (int i = 0; i < num_tables; i++) {
    clean_table(&tables[i]);
  }
  free(tables);
  return 0;
}

// Returns a party size of between 1-12 (implicitly assuming largest party is 12)
size_t get_party() 
{
  // No zero size parties!
  size_t partysz; 
  do {
    partysz = (poissonRandom(4.0) % 12) + 1;
  } while (0 == partysz);
  if (VERBOSE)
    printf("Party of size %lu just arrived to %s!\n", partysz, restaurant_name);
  return partysz;
}

size_t seat_guests(struct table *table, size_t party_sz)
{
  table->num_seated = 0;
  table->guests = (person *) malloc(sizeof(person) * party_sz);
  if (!table->guests) {
    fprintf(stderr, "Unable to allocate party to table!\n");
    return 0;
  }
  table->num_seated = party_sz;

  return table->num_seated;
}

// Creates a mapping array that maps from tables -> parties for seating assignments once all parties are present
// Assumes num_tables >= toseat
int * calculate_seating_assignments(struct table *tables, unsigned int num_tables, size_t *parties, unsigned int toseat)
{
  int *mapping = (int *) malloc(sizeof(int) * num_tables);

  if (0 == mapping) {
    fprintf(stderr, "Unable to allocation seating assignments!\n");
    exit(-1);
  }

  // Initialize mappings to invalid (-1)
  for (unsigned int i = 0; i < num_tables; i++) {
    mapping[i] = -1;
  }

  // Assume there is enough space
#pragma omp parallel
#pragma omp for
  for (unsigned int i = 0; i < toseat; i++) {
    int ssz = parties[i] - 1;
    //printf("Mapping party size: %lu\n", parties[i]);
    unsigned char found = 0;
    do {
      ssz++;
      for (unsigned int j = 0; j < num_tables; j++) {
	if (mapping[j] == -1) {
	  assert(tables[j].num_seated == 0);
	  if (tables[j].capacity == ssz) {
	    #pragma omp critical 
	    {
	      mapping[j] = i;
	      if (VERBOSE) 
		printf("Mapping party size %lu to table (%d) size %d\n", parties[i], j, tables[j].capacity);
	      found = 1;
	    }
	    break;
	  }
	}
      }

    } while (found == 0 && ssz < 13);

    if (0 == found || ssz > 12) {
      fprintf(stderr, "Unable to find seating for party of size %lu!\n", parties[i]);
    }
  }

  // TODO: Allow for "upgrading" of parties to larger tables if needed to make room for smaller, less efficient mappings

  return mapping;
}

unsigned int is_satisfiable(struct table *tables, unsigned int num_tables, size_t *parties, unsigned int toseat) 
{
  int *mapping = (int *) malloc(sizeof(int) * num_tables);
  if (0 == mapping) {
    fprintf(stderr, "Unable to allocation seating assignments!\n");
    exit(-1);
  }

  // Initialize mappings to invalid (-1)
  for (unsigned int i = 0; i < num_tables; i++) {
    mapping[i] = -1;
  }

  // Assume there is enough space
  for (unsigned int i = 0; i < toseat; i++) {
    int ssz = parties[i] - 1;
    unsigned char found = 0;
    do {
      ssz++;
      for (unsigned int j = 0; j < num_tables; j++) {
	if (mapping[j] == -1) {
	  assert(tables[j].num_seated == 0);
	  if (tables[j].capacity == ssz) {
	    mapping[j] = i;
	    found = 1;
	    break;
	  }
	}
      }

    } while (found == 0 && ssz < 13);

    if (0 == found || ssz > 12) {
      free(mapping);
      fprintf(stderr, "Unable to find seating for party of size %lu ssz: %d!\n", parties[i], ssz);      
      return i;
      
    }
  }

  // TODO: Allow for "upgrading" of parties to larger tables if needed to make room for smaller, less efficient mappings
  free(mapping);
  return toseat;
}

// Free()s the guests at a party and resets it to be empty
void clean_table(struct table *table)
{
  if (table->num_seated) {
    free(table->guests);
    table->num_seated = 0;
  }
}

// Setups the restaurant for dining
size_t setup_tables(struct table *tables, size_t num_tables) 
{
  size_t tables_setup = 0;
  for (unsigned int i = 0; i < num_tables; i++) {
    if (tables[i].capacity == 0) {
      tables[i].capacity = max(2, (poissonRandom(6.0) % 12) + 1);
    }
    tables[i].num_seated = 0;
    tables[i].guests = NULL;
    tables_setup++;
    if (VERBOSE) 
      printf("Set table of size %d\n", tables[i].capacity);
  }

  return tables_setup;
}

unsigned int total_space = 0, used_space = 0;

// Prints out the stats for the night at the restaurant
void print_state(struct table *tables, size_t num_tables)
{
  float eff;
  for (unsigned int i = 0; i < num_tables; i++) {
    if (0 != tables[i].num_seated) {
      total_space += tables[i].capacity;
      used_space += tables[i].num_seated;
    }
  }
  eff = (float) used_space / (float) total_space;
  printf("Cumulative restaurant state: %u seats filled of %u total seats made unusable (%f%% efficiency)\n", used_space, total_space, eff * 100.0);
}

int poissonRandom(double expectedValue) 
{
  int n = 0; //counter of iteration
  double limit; 
  double x;  //pseudo random number
  limit = exp(-expectedValue);
  x = rand() / (double) INT_MAX; 

  while (x > limit) {
    n++;
    x *= rand() / (double) INT_MAX;
  }
  return n;
}

int min(int a, int b)
{
  return (a < b) ? a : b;
}

int max(int a, int b)
{
  return (a > b) ? a : b;
}
