#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>
#include <math.h>
#include <limits.h>
#include <string.h>

#define PRANDOM 0
#define VERBOSE 0
#define DEFAULT_NUM_TABLES 10
typedef char person;

struct table {
  int capacity; // Implicitly 2
  int num_seated;
  person *guests;
};

const char *restaurant_name = "Chez Jacob";

int poissonRandom(double expectedValue);
int min(int a, int b);

size_t setup_tables(struct table *tables, size_t num_tables);
size_t get_party();
size_t seat_guests(struct table *table, size_t party_sz);
void clean_table(struct table *table);
void print_state(struct table *tables, size_t num_tables);

int main(int argc, char ** argv) 
{
  unsigned int num_tables = DEFAULT_NUM_TABLES;
  struct table *tables = NULL;
  unsigned int num_parties, seated = 0, curr_table = 0;

  if (PRANDOM)
    srand(time(0));

  if (argc == 2) {
    num_tables = atoi(argv[1]);
  }
  // Assume there are no more than 4 full seatings per night
  num_parties = rand() % (4 * num_tables * 2);
  size_t *parties = (size_t *) malloc(sizeof(size_t) * num_parties);

#pragma omp parallel
#pragma omp for
  for (unsigned int i = 0; i < num_parties; i++) {
    parties[i] = get_party();
  }

  printf("Allocating %d tables for %s\n", num_tables, restaurant_name);
  tables = (struct table *) malloc(sizeof(struct table) * num_tables);
  if (!tables) {
    fprintf(stderr, "Unable to allocate restaurant!\n");
    return -1;
  }

  printf("The restaurant is open!\n");
  printf("There are %d parties lined up to eat\n", num_parties); 
  do {
    setup_tables(tables, num_tables);
    
    curr_table = 0;
    while(num_parties - seated != 0) {

      if (num_tables - curr_table < ceil((float) parties[seated] / 2.0)) {
	if (VERBOSE)
	  printf("Not enough spare tables for this seating!\n");
	break;
      }

      if (VERBOSE) 
	printf("Seating party of size %lu\n", parties[seated]);

      size_t tseated = seat_guests(&tables[curr_table], parties[seated]);
      if (tseated != parties[seated]) {
	if (VERBOSE)
	  fprintf(stderr, "Party of size %lu was not properly seated (only %lu seated)!\n", parties[seated], tseated);
	goto error;
      }
      
      curr_table += (int) ceil((float) parties[seated] / 2.0);
      seated++;
    }

    if (VERBOSE)
      printf("Seated %d parties so far, %d in waiting area!\n", seated, num_parties - seated);
    print_state(tables, num_tables);
    
#pragma omp parallel
#pragma omp for
    for (int i = 0; i < num_tables; i++) {
      clean_table(&tables[i]);
    }

  } while (0 != num_parties - seated);

 error:
  for (int i = 0; i < num_tables; i++) {
    clean_table(&tables[i]);
  }
  free(tables);
  free(parties);
  return 0;
}

// Returns a party size of between 1-16 (implicitly assuming largest party is 16)
size_t get_party() 
{
  // No zero size parties!
  size_t partysz; 
  do {
    partysz = (poissonRandom(4.0) % 16) + 1;
  } while (0 == partysz);
  if (VERBOSE) 
    printf("Party of size %lu just arrived to %s!\n", partysz, restaurant_name);
  return partysz;
}

size_t seat_guests(struct table *table, size_t party_sz)
{
  size_t seated = 0;
  unsigned int ntables = 0;
  
  if (VERBOSE)
    printf("Seating party of %lu ", party_sz);

  do {
    ntables++;
    unsigned int toseat = min(table->capacity, party_sz - seated);
    table->num_seated = toseat;
    seated += toseat;
    table++;
  } while(seated != party_sz);

  if (VERBOSE)
    printf("by pushing together %d tables of size 2\n", ntables);

  return seated;
}

// Free()s the guests at a party and resets it to be empty
void clean_table(struct table *table)
{
  if (0 != table->num_seated) {
    table->num_seated = 0;
  }
}

// Setups the restaurant for dining
size_t setup_tables(struct table *tables, size_t num_tables) 
{
  size_t tables_setup = 0;
  for (unsigned int i = 0; i < num_tables; i++) {
    tables[i].capacity = 2;
    tables[i].num_seated = 0;
    tables[i].guests = NULL;
    tables_setup++;
  }

  return tables_setup;
}

unsigned int total_space = 0, used_space = 0;

// Prints out the stats for the night at the restaurant
void print_state(struct table *tables, size_t num_tables)
{
  float eff;
  for (unsigned int i = 0; i < num_tables; i++) {
    if (0 != tables[i].num_seated) {
      total_space += tables[i].capacity;
      used_space += tables[i].num_seated;
    }
  }
  eff = (float) used_space / (float) total_space;
  printf("Cumulative restaurant state: %u seats filled of %u total seats made unusable (%f%% efficiency)\n", used_space, total_space, eff * 100.0);
}

int poissonRandom(double expectedValue) 
{
  int n = 0; //counter of iteration
  double limit; 
  double x;  //pseudo random number
  limit = exp(-expectedValue);
  x = rand() / (double) INT_MAX; 

  while (x > limit) {
    n++;
    x *= rand() / (double) INT_MAX;
  }
  return n;
}

int min(int a, int b)
{
  return (a < b) ? a : b;
}
